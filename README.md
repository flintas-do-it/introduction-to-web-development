# Example project for the introduction of web development

This project contains the code of our example page for the FLINTA*s coding course in introduction in web development.

The page resembles a social media feed. For a preview of the page, click the logo below:

[![FLINTAGRAM logo](images/logo.svg)](https://flintas-do-it.gitlab.io/introduction-to-web-development)